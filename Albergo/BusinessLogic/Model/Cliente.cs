﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Model
{
    public class Cliente : Persona
    {
        public int IdCliente { get; set; }
        public String CodigoPostal { get; set; }
        public String Direccion { get; set; }
        public String Estado { get; set; }
        public String Nacionalidad { get; set; }
        public String RFC { get; set; }
    }
}
