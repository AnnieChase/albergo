﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Model
{
    public class Cotizacion
    {
        public int IdCotizacion { get; set; }
        public int NumeroPersonas { get; set; }
        public int NochesEstancia { get; set; }
        public String TipoHabitacion { get; set; }
        public float CostoTotal { get; set; }
    }
}
