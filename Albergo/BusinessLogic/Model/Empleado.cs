﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Model
{
    public class Empleado : Persona
    {
        public int IdEmpleado { get; set; }
        public int ClavePersonal { get; set; }
    }
}
