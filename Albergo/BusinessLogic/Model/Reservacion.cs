﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Model
{
    public class Reservacion
    {
        public int IdReservacion { get; set; }
        public DateTime FechaEntrada { get; set; }
        public DateTime FechaReservacion { get; set; }
        public int Noches { get; set; }
        public int Ocupado { get; set; }
        public int NumeroPersonas { get; set; }
        public double CostoCalculado { get; set; }
        List<Habitacion> habitaciones;
        
    }
}
