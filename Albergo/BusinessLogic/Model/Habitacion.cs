﻿
using BusinessLogic.Model.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Model
{
    public class Habitacion
    {
        public int Tamano { get; set; }
        public ETipoHabitacion TipoHabitacion { get; set; }
        public ETipoTerraza Terraza { get; set; }
        public EEstiloHabitacion Estilo { get; set; }
        public string Vista { get; set; }
        public int NumeroHabitacion { get; set; }
        public string IdHabitacion { get; set; }
        public double Costo { get; set; }
    }
}
