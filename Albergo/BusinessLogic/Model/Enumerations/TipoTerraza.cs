﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.Model.Enumerations
{
    public enum ETipoTerraza
    {
        Interior,
        Espaciosa,
        Extragrande,
        Veranda
    }
    
}
