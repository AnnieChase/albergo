﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.DataAccess.Interfaces;
using BusinessLogic.Model;
using System.Data;
using Persistence;

namespace BusinessLogic.DataAccess
{
    public class PersonaDAO : BaseDAO, IPersonaDAO
    {
        public PersonaDAO()
        {

        }


        public PersonaDAO(Transaction transaction)
        {
            this.transaction = transaction;
        }

        public int AgregarPersona(Persona persona)
        {
            var command = new Command("INSERT INTO persona (nombre, apellidoPaterno, apellidoMaterno, email, telefono) " +
                "VALUES (@nombre, @apellidoPaterno, @apellidoMaterno, @email, @telefono);");

            command.AddParameter("@nombre", persona.Nombre);
            command.AddParameter("@apellidoPaterno", persona.ApellidoPaterno);
            command.AddParameter("@apellidoMaterno", persona.ApellidoMaterno);
            command.AddParameter("@email", persona.Email);
            command.AddParameter("@telefono", persona.Telefono);

            var result = command.Execute();

            if (result == 0)
            {
                throw new Exception("No se pudo agregar a la persona");
            }

            var idPersona = command.GetLastInsertId();

            return idPersona;

        }

        public void EditarPersona(Persona persona)
        {
            var command = new Command("UPDATE persona SET " +
                "nombre = @nombre, apellidoPaterno = @apellidoPaterno, apellidoMaterno = @apellidoMaterno, " +
                "email = @email, telefono = @telefono" +
                "WHERE idPersona = idPersona; ");

            var result = command.Execute();

            if (result == 0)
            {
                throw new Exception("Error al modificar la persona con ID: " + persona.IdPersona);
            }
        }

        public List<Persona> GetPersonas()
        {
            List<Persona> personas = new List<Persona>();

            var command = Command(
                "SELECT (nombre,apellidoPaterno,apellidoMaterno,email,telefono) FROM persona;");

            var dataTable = command.Query();

            foreach (DataRow row in dataTable.Rows)
            {
                var persona = new Persona
                {
                    Nombre = row.Field<String>("nombre"),
                    ApellidoPaterno = row.Field<String>("apellidoPaterno"),
                    ApellidoMaterno = row.Field<string>("apellidoMaterno"),
                    Email = row.Field<string>("email"),
                    Telefono = row.Field<string>("telefono"),

                };
                personas.Add(persona);
            }
            return personas;
        }
    }
}
