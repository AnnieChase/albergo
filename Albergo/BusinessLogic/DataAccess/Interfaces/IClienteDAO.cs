﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interfaces
{
    public interface IClienteDAO
    {
        int Agregarcliente(Cliente cliente);
        int EditarCliente(Cliente cliente);
        List<Cliente> GetClientes();
    }
}
