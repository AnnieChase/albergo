﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Model;

namespace BusinessLogic.DataAccess.Interfaces
{
    public interface ICotizacionDAO
    {
        int AgregarCotizacion(Cotizacion cotizacion);
        List<Cotizacion> GetCotizaciones();
    }
}
