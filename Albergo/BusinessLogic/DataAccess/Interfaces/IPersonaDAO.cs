﻿using System;
using System.Collections.Generic;
using BusinessLogic.Model;
namespace BusinessLogic.DataAccess.Interfaces
{
    public interface IPersonaDAO
    {
        int AgregarPersona(Persona persona);
        void EditarPersona(Persona persona);
        List<Persona> GetPersonas();
    }
}
