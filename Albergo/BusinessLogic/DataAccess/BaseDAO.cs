﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Persistence;

namespace BusinessLogic.DataAccess
{
    public abstract class BaseDAO
    {
            internal Transaction transaction;

            internal Command Command(string query)
            {
                if (transaction != null)
                {
                    return transaction.Command(query);
                }
                else
                {
                    return new Command(query);
                }
            }
    }
}
