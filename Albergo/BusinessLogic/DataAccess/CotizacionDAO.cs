﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.DataAccess.Interfaces;
using BusinessLogic.Model;
using Persistence;

namespace BusinessLogic.DataAccess
{
    public class CotizacionDAO : BaseDAO, ICotizacionDAO
    {
        public CotizacionDAO()
        {

        }

        public CotizacionDAO(Transaction transaction)
        {
            this.transaction = transaction;
        }

        public int AgregarCotizacion(Cotizacion cotizacion)
        {
            var command = Command("INSERT INTO Cotizacion NumeroPersonas, NochesEstancia, TipoHabitacion, CostoTotal " +
                "VALUES (@NumeroPersonas, @NochesEstancia, @TipoHabitacion, @CostoHabitacion, @CostoTotal);");

            command.AddParameter("@NumeroPersonas",cotizacion.NumeroPersonas);
            command.AddParameter("@NochesEstancia",cotizacion.NochesEstancia);
            command.AddParameter("@TipoHabitacion",cotizacion.TipoHabitacion);
            command.AddParameter("@CostoTotal",cotizacion.CostoTotal);

            var result = command.Execute();

            if (result == 0)
            {
                throw new Exception("No se pudo agregar la cotización");
            }

            var idCotizacion = command.GetLastInsertId();

            return idCotizacion;
        }

        public List<Cotizacion> GetCotizaciones()
        {
            throw new NotImplementedException();
        }
    }
}
