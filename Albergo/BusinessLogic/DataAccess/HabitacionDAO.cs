﻿using BusinessLogic.Model;
using BusinessLogic.Model.Enumerations;
using Persistence;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic.DataAccess
{
    public class HabitacionDAO
    {
        public List<Habitacion> RecuperarHabitaciones()
        {
            List<Habitacion> ListaHabitaciones = new List<Habitacion>();
            DBlecture dBLecture = new DBlecture();
            using (SqlConnection connection = dBLecture.CreateConnection())
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("SELECT * FROM habitacion;", connection))
                {
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Habitacion habitacion = new Habitacion();
                        habitacion.IdHabitacion = reader["idHabitacion"].ToString();
                        habitacion.Estilo = (EEstiloHabitacion)Enum.Parse(typeof(EEstiloHabitacion), reader["estilo"].ToString());
                        habitacion.Tamano = Int16.Parse(reader["tamanio"].ToString());
                        habitacion.Terraza = (ETipoTerraza)Enum.Parse(typeof(ETipoTerraza), reader["terraza"].ToString());
                        habitacion.TipoHabitacion = (ETipoHabitacion)Enum.Parse(typeof(ETipoHabitacion), reader["tipoHabitacion"].ToString());
                        habitacion.Vista = reader["vista"].ToString();
                        habitacion.Costo = Double.Parse(reader["costo"].ToString());
                        ListaHabitaciones.Add(habitacion);
                    }

                }
                connection.Close();
            }
            return ListaHabitaciones;
        }
    }
}
