﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.DataAccess.Interfaces;
using BusinessLogic.Model;
using System.Data;
using Persistence;

namespace BusinessLogic.DataAccess
{
    public class ReservacionDAO : BaseDAO
    {
        public ReservacionDAO()
        {
        }

        public ReservacionDAO(Transaction transaction)
        {
            this.transaction = transaction;
        }
        public int AgregarReservacion(Reservacion reservacion)
        {
            var command = new Command("INSERT INTO reservacion (fechaReservacion, noches, costoCalculado) " +
                "VALUES (@fechaReservacion, @noches, @costoCalculado );");

            
            command.AddParameter("@fechaReservacion", reservacion.FechaReservacion);
            command.AddParameter("@noches", reservacion.Noches);
            command.AddParameter("@costoCalculado", reservacion.CostoCalculado);

            var result = command.Execute();

            if (result == 0)
            {
                throw new Exception("No se pudo agregar la reservacion");
            }

            var idReservacion = command.GetLastInsertId();

            return idReservacion;

        }

        public void EditarReservacion(Reservacion reservacion)
        {
            var command = new Command("UPDATE Reservacion SET " +
                "FechaEntrada = @FechaEntrada, FechaReservacion = @FechaReservacion, Noches = @Noches, " +
                "Ocupado = @Ocupado, CostoCalculado = @CostoCalculado" +
                "WHERE IdReservacion = IdReservacion; ");

            var result = command.Execute();

            if (result == 0)
            {
                throw new Exception("Error al modificar la Reservacion con ID: " + reservacion.IdReservacion);
            }
        }

        public List<Reservacion> GetReservacion()
        {
            List<Reservacion> reservacions = new List<Reservacion>();

            var command = Command(
                "SELECT R.FechaEntrada, " +
                "R.FechaReservacion, " +
                "R.Noches, " +
                "R.Ocupado, " +
                "R.CostoCalculado " +
                "FROM Reservacion AS R;"
                );

            var dataTable = command.Query();

            foreach (DataRow row in dataTable.Rows)
            {
                var reserva = new Reservacion
                {
                    FechaEntrada = row.Field<DateTime>("FechaEntrada"),
                    FechaReservacion = row.Field<DateTime>("FechaReservacion"),
                    Noches = row.Field<int>("Noches"),
                    Ocupado = row.Field<int>("Ocupado"),
                    CostoCalculado = row.Field<float>("CostoCalculado"),

                };
                reservacions.Add(reserva);
            }
            return reservacions;
        }
    }
}
