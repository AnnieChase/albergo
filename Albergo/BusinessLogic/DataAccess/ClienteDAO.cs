﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.DataAccess.Interfaces;
using BusinessLogic.Model;
using Persistence;

namespace BusinessLogic.DataAccess
{
    public class ClienteDAO : BaseDAO,IClienteDAO
    {

        public ClienteDAO()
        {

        }
        public ClienteDAO(Transaction transaction)
        {
            this.transaction = transaction;
        }
        public int Agregarcliente(Cliente cliente)
        {
            bool pendingTransaction = false;
            var transaction = this.transaction;
            if (this.transaction == null)
            {
                transaction = new Transaction();
            }
            else
            {
                pendingTransaction = true;
            }

            var personaDAO = new PersonaDAO();

            var idper = personaDAO.AgregarPersona(cliente);
            if (cliente.CodigoPostal != null)
            {

                var command = Command("INSERT INTO cliente (codigoPostal,direccion,estado,nacionalidad,rfc,idPersona) " +
                    "VALUES(codigoPostal,@direccion,@estado,@nacionalidad,@rfc,@idPersona) ;");

                command.AddParameter("@codigoPostal", cliente.CodigoPostal);
                command.AddParameter("@direccion", cliente.Direccion);
                command.AddParameter("@estado", cliente.Estado);
                command.AddParameter("@nacionalidad", cliente.Nacionalidad);
                command.AddParameter("@rfc", cliente.RFC);
                command.AddParameter("@idPersona", idper);
                var result = command.Execute();
                if (result == 0)
                {
                    throw new Exception("No se pudo agregar el cliente.");
                }
                var idCliente = command.GetLastInsertId();

                return idCliente;
            }
            else
            {
                var command = Command("INSERT INTO cliente (nacionalidad,idPersona) " +
                   "VALUES(@nacionalidad,@idPersona) ;");

                command.AddParameter("@nacionalidad", cliente.Nacionalidad);
                command.AddParameter("@idPersona", idper);
                var result = command.Execute();
                if (result == 0)
                {
                    throw new Exception("No se pudo agregar el cliente.");
                }
                var idCliente = command.GetLastInsertId();
                return idCliente;
            }            
        }

        public int EditarCliente(Cliente cliente)
        {

            bool pendingTransaction = false;
            var transaction = this.transaction;
            if (this.transaction == null)
            {
                transaction = new Transaction();
            }
            else
            {
                pendingTransaction = true;
            }

            var personaDAO = new PersonaDAO(transaction);

            // TODO catch
            personaDAO.EditarPersona(cliente);

            var command = Command("UPDATE Cliente" +
            "SET idPersona = @idPersona, " +
            "CodigoPostal = @CodigoPostal, " +
            "Direccion = @Direccion, " +
            "Estado = @Estado, " +
            "Nacionalidad = Nacionalidad, " +
            "RFC = @RFC " +
            "WHERE idPersona = @idPersona;");

            var result = command.Execute();
            if (result == 0)
            {
                throw new Exception("No se pudo editar el cliente.");
            }

            var idCliente = command.GetLastInsertId();

            if (!pendingTransaction)
            {
                transaction.Commit();
            }

            return idCliente;
        }

        public List<Cliente> GetClientes()
        {
            List<Cliente> clientes = new List<Cliente>();

            var command = Command("SELECT (P.IdPersona, C.idCliente, P.nombre, P.apellidoPaterno, P.apellidoMaterno, P.email, P.telefono, C.codigoPostal, C.direccion, C.estado, C.nacionalidad, C.rfc)" +
                "FROM cliente AS C LEFT JOIN persona AS P ON C.idPersona = P.idPersona;");

            var dataTable = command.Query();

            foreach (DataRow dataRow in dataTable.Rows)
            {
                var cliente = new Cliente
                {
                    IdPersona = dataRow.Field<int>("idPersona"),
                    Nombre = dataRow.Field<string>("nombre"),
                    ApellidoPaterno = dataRow.Field<string>("apellidoPaterno"),
                    ApellidoMaterno = dataRow.Field<string>("apellidoMaterno"),
                    Email = dataRow.Field<string>("email"),
                    Telefono = dataRow.Field<string>("telefono"),
                    IdCliente = dataRow.Field<int>("idCliente"),
                    CodigoPostal = dataRow.Field<String>("codigoPostal"),
                    Direccion = dataRow.Field<string>("direccion"),
                    Estado = dataRow.Field<string>("estado"),
                    RFC = dataRow.Field<string>("rfc")
                };

                clientes.Add(cliente);
            }
            return clientes;
        }
    }
}
