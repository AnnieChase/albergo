﻿using System;
using System.Data.SqlClient;

namespace Persistence
{
    public class Transaction
    {
        private SqlTransaction sqlTransaction;

        public Transaction()
        {
            sqlTransaction = DB.Instance.CreateTransaction();
        }

        public Command Command(string query)
        {
            return new Command(query, sqlTransaction);
        }

        public void Commit()
        {
            sqlTransaction.Commit();
        }

        public void Rollback()
        {
            sqlTransaction.Rollback();
        }
    }
}
