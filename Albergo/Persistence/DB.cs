﻿using System.Configuration;
using System.Data.SqlClient;


namespace Persistence
{
    public class DB
    {

        private DB()
        {

        }
        /// <summary>
        /// Única instancia de la clase DB
        /// </summary>
        public static DB Instance { get; } = new DB();

        /// <summary>
        /// Devuelve la única conexión SQL de la base de datos SQL Server. La inicializa si es necesario.
        /// </summary>
        /// <returns>Conexión SQL de la base de datos SQL Server.</returns>
        private SqlConnection CreateConnection()
        {
            var connectionString = Properties.Settings.Default.CadenaConexion;
            var sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();

            return sqlConnection;
        }

        public SqlCommand Command(string query)
        {
            return new SqlCommand(query, CreateConnection());
        }
        public SqlCommand Command(string query, SqlTransaction sqlTransaction)
        {
            return new SqlCommand(query, sqlTransaction.Connection, sqlTransaction);
        }

        public SqlCommand Command(string query, SqlConnection sqlConnection, SqlTransaction sqlTransaction)
        {
            return new SqlCommand(query, sqlConnection, sqlTransaction);
        }

        public SqlTransaction CreateTransaction()
        {
            return CreateConnection().BeginTransaction();
        }
    }
}
