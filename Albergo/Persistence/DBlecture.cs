﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence
{
    public class DBlecture
    {
        public SqlConnection CreateConnection()
        {
            var connectionString = Properties.Settings.Default.CadenaConexion;
            var sqlConnection = new SqlConnection(connectionString);
            return sqlConnection;
        }
    }
}
