﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence
{
    public class Command
    {
        private SqlCommand command;

        public Command(string query)
        {
            command = DB.Instance.Command(query);
        }

        public Command(string query, SqlTransaction sqlTransaction)
        {
            command = DB.Instance.Command(query);
        }

        public void AddParameter(string parameterName, object value)
        {
            command.Parameters.AddWithValue(parameterName, value);
        }

        public int GetLastInsertId()
        {
            command = DB.Instance.Command("SELECT @@IDENTITY", command.Connection, command.Transaction);
            return Convert.ToInt32(command.ExecuteScalar());
        }

        public DataTable Query()
        {
            var sqLiteDataAdapter = new SqlDataAdapter(command);
            var dataTable = new DataTable();
            sqLiteDataAdapter.Fill(dataTable);
            return dataTable;
        }

        /// <summary>
		///// Ejecuta una instrucción INSERT, UPDATE o DELETE en la base de datos
		/// </summary>
		/// <returns>El número de filas afectadas</returns>
		public int Execute()
        {
            return command.ExecuteNonQuery();
        }

    }
}
