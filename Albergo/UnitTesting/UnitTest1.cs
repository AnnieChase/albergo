﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic.Model;
using BusinessLogic.DataAccess;

namespace UnitTesting
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var reservacion = new Reservacion();
            var reservacionDAO = new ReservacionDAO();
            var cliente = new Cliente();
            var clienteDAO = new ClienteDAO();
            var persona = new Persona();
            var personaDAO = new PersonaDAO();

            reservacion.Noches = 2;
            reservacion.FechaReservacion = DateTime.Today;
            reservacion.Ocupado = 1;
            reservacion.CostoCalculado = 300;
            reservacionDAO.AgregarReservacion(reservacion);
            persona.Nombre = "Delmer";
            persona.ApellidoMaterno = "Lopez";
            persona.ApellidoPaterno = "Hernandez";
            persona.Telefono = "921123"; // Cambiar el valor del campo para que sea entero y maximo 10 digitos
            persona.Email = "Delmer@hotmail.com";
            cliente.Nacionalidad = "Mexicano";
            //Si esta activo

            cliente.RFC = "D91283";
            cliente.Direccion = "Azueta ";
            cliente.Estado = "Veracruz";
            cliente.CodigoPostal = "91020";
            clienteDAO.Agregarcliente(cliente);

        }
    }
}
