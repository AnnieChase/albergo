﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic.DataAccess;
using BusinessLogic.Model;
using Persistence;

namespace UnitTesting
{
    [TestClass]
    public class UnitTest1 : BaseDAOTest
    {
        [TestMethod]
        public void TestAgregarCliente()
        {
            var cliente = new Cliente()
            {
                Nombre = "Alfredo",
                ApellidoPaterno = "Sanchez",
                ApellidoMaterno = "Montes",
                Email = "alfredo123@panoramicas.com",
                Telefono = "123413113",
                CodigoPostal = "96752",
                Direccion = "p sherman calle wallaby 42",
                Estado = "Tlaxcala",
                Nacionalidad = "Tlaxcalense",
                RFC = "1020123123231"
            };

            var clienteDAO = new ClienteDAO(sqlTransaction);
            var idCliente = clienteDAO.Agregarcliente(cliente);
            Assert.IsTrue(idCliente > 0);
        }

        [TestMethod]
        public void TestGetClientes()
        {           
            var cliente = new Cliente()
            {
                Nombre = "Alfredo",
                ApellidoPaterno = "Sanchez",
                ApellidoMaterno = "Montes",
                Email = "alfredo123@panoramicas.com",
                Telefono = "123413113",
                CodigoPostal = "96752",
                Direccion = "p sherman calle wallaby 42",
                Estado = "Tlaxcala",
                Nacionalidad = "Tlaxcalense",
                RFC = "1020123123231"
            };

            var clienteDAO = new ClienteDAO(sqlTransaction);
            var idCliente = clienteDAO.Agregarcliente(cliente);
            var clientes = clienteDAO.GetClientes();

            Assert.IsTrue(clientes.Count > 0);
        }
    }
}
