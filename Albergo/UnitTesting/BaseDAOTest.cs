﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic.DataAccess;
using Persistence;
using System.Data.SqlClient;

namespace UnitTesting
{
    public abstract class BaseDAOTest : IDisposable
    {
        internal Transaction sqlTransaction;

        protected BaseDAOTest()
        {
            sqlTransaction = new Transaction();
        }

        public void Dispose()
        {
            sqlTransaction.Rollback();
        }
    }
}
