﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic.DataAccess;
using BusinessLogic.Model;

namespace UnitTesting
{
    [TestClass]
    public class PersonaDAOTest : BaseDAOTest
    {
        [TestMethod]
        public void TestAgregarPersona()
        {
            var persona = new Persona
            {
                Nombre = "Eduardo Josue",
                ApellidoPaterno = "Cortes",
                ApellidoMaterno = "Gomez",
                Email = "ejcg22@hotmail.com",
                Telefono = "123413113"
            };

            var personaDAO = new PersonaDAO(sqlTransaction);
            var idPersona = personaDAO.AgregarPersona(persona);
            Assert.IsTrue(idPersona > 0);
        }
        [TestMethod]
        public void TestGetPersonas()
        {
            var persona = new Persona
            {
                Nombre = "Eduardo Josue",
                ApellidoPaterno = "Cortes",
                ApellidoMaterno = "Gomez",
                Email = "ejcg22@hotmail.com",
                Telefono = "123413113"
            };

            var personaDAO = new PersonaDAO(sqlTransaction);
            var idPersona = personaDAO.AgregarPersona(persona);
            var personas = personaDAO.GetPersonas();

            Assert.IsTrue(personas.Count > 0);
        }
    }
}
