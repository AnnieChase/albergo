﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Model;


namespace Albergo.Utils
{
    public class Sesion
    {
        private static Empleado Empleado;

        public static void SetEmpleado(Empleado empleado)
        {
            Empleado = empleado;
        }

        public static int GetEmpelado()
        {
            return Empleado.IdEmpleado;

        }
    }
}
