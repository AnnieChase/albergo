﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessLogic.Model;
using BusinessLogic.DataAccess;
using Persistence;
using System.Text.RegularExpressions;

namespace Albergo
{
    /// <summary>
    /// Lógica de interacción para NuevaReservacion2.xaml
    /// </summary>
    public partial class NuevaReservacion : Page
    {
        public ListView habitaciones;
        public List<Habitacion> ListaHabitaciones = new List<Habitacion>();

        public NuevaReservacion()
        {
            InitializeComponent();
            FormatearFecha();
            FormatearLista();
            habitaciones = HabitacionesListView;
            FechaReservacionPicker.Text = FechaReservacionPicker.DisplayDate.ToString();
            FechaReservacionPicker.IsEnabled = true;
        }

        public NuevaReservacion(List<Habitacion> ListaHabitaciones)
        {
            InitializeComponent();
            FormatearFecha();
            FormatearLista();
            habitaciones = HabitacionesListView;
            this.ListaHabitaciones = ListaHabitaciones;
            Actualizar();
        }

        private void ReservarHabitacionButton_Click(object sender, RoutedEventArgs e)
        {
            ReservarHabitacionButton.IsEnabled = false;
            RegistrarNuevaReserva();
            ReservarHabitacionButton.IsEnabled = true;
        }
        public void RegistrarNuevaReserva()
        {
            var reservacionDAO = new ReservacionDAO();
            var clienteDAO = new ClienteDAO();

            if (validarCamposCliente())
            {
                if (EsEmailValido(EmailTextBox.Text))
                {
                    if (EsNumero(TelefonoTextBox.Text))
                    {
                        if (validarCamposReservacion())
                        {
                            reservacionDAO.AgregarReservacion(AsignarReserva());
                            clienteDAO.Agregarcliente(AsignarCliente());                            
                            LimpiarCampos();
                            MessageBox.Show("Registro de reservacion exitoso ", "Exito ", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show("No se rellenaron todos los campos de la reservacion", "Error al registrar reservacion", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }                        
                    }
                    else
                    {
                        TelefonoTextBox.Text = "";
                        MessageBox.Show("Se intodujo numero no valido", "Error al registrar cliente", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }                    
                }
                else
                {
                    EmailTextBox.Text = "";
                    MessageBox.Show("Se intodujo un E-mail no valido", "Error al registrar cliente", MessageBoxButton.OK, MessageBoxImage.Warning);
                }

            }
            else
            {
                MessageBox.Show("No se rellenaron todos los campos de cliente", "Error al registrar cliente", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            

        }
        public Reservacion AsignarReserva()
        {
            var reservacion = new Reservacion();

            reservacion.Noches = int.Parse(NumeroNochesTextBox.Text.ToString());
            reservacion.FechaReservacion= Convert.ToDateTime(FechaReservacionPicker.ToString());
            reservacion.Ocupado = 1;
            
            return reservacion;
        }
        public Cliente AsignarCliente()
        {

            var cliente = new Cliente()
            {
                Nombre = NombreTextBox.Text,
                ApellidoPaterno = ApellidoMaternoTextBox.Text,
                ApellidoMaterno = ApellidoPaternoTextBox.Text,
                Email = EmailTextBox.Text,
                Telefono = TelefonoTextBox.Text,
                Nacionalidad = NacionalidadTextBox.Text,
            };
            if (FacturaCheckBox.IsChecked == true)
            {
                cliente.RFC = RFCTextBox.Text;
                cliente.Direccion = DireccionTextBox.Text;
                cliente.Estado = EstadoTextBox.Text;
                cliente.CodigoPostal = CodigoPostalTextBox.Text;
            }                      
            return cliente;
        }

        public void FormatearFecha()
        {
            CultureInfo ci = CultureInfo.CreateSpecificCulture(CultureInfo.CurrentCulture.Name);
            ci.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd";
            Thread.CurrentThread.CurrentCulture = ci;
        }

        private void AnadirHabitacionButton_Click(object sender, RoutedEventArgs e)
        {
            ListarHabitaciones listarHabitaciones = new ListarHabitaciones(this, ListaHabitaciones);
            listarHabitaciones.ShowDialog();
            Limpiar();
            Actualizar();
        }

        public bool validarCamposReservacion()
        {
            if (!String.IsNullOrEmpty(NumeroNochesTextBox.Text.ToString()))
            {
                if (!String.IsNullOrEmpty(TotalTextBox.Text))
                {
                    return true;
                }
            }
            return false;
        }
        public bool validarCamposCliente()
        {
            if (!String.IsNullOrEmpty(NombreTextBox.Text))
            {
                if (!String.IsNullOrEmpty(ApellidoMaternoTextBox.Text))
                {
                    if (!String.IsNullOrEmpty(ApellidoPaternoTextBox.Text))
                    {
                        if (!String.IsNullOrEmpty(TelefonoTextBox.Text))
                        {
                            if (!String.IsNullOrEmpty(EmailTextBox.Text))
                            {
                                if (!String.IsNullOrEmpty(NacionalidadTextBox.Text))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        } 
        public bool validar()
        {
            if (EsNombre(NombreTextBox.Text))
            {
                if (EsNombre(ApellidoMaternoTextBox.Text))
                {
                    if (EsNombre(ApellidoPaternoTextBox.Text))
                    {
                        if (EsNumero(TelefonoTextBox.Text))
                        {
                            if (EsEmailValido(EmailTextBox.Text))
                            {
                                if (EsNacionalidad(NacionalidadTextBox.Text) && EsNombre(NacionalidadTextBox.Text))
                                {
                                    return true;
                                }
                                else
                                {
                                    NacionalidadTextBox.Text = "";
                                    MessageBox.Show("Se intodujo una nacionalidad no valido", "Error al registrar cliente", MessageBoxButton.OK, MessageBoxImage.Warning);
                                }
                                EmailTextBox.Text = "";
                                MessageBox.Show("Se intodujo un email no valido", "Error al registrar cliente", MessageBoxButton.OK, MessageBoxImage.Warning);
                            }
                            TelefonoTextBox.Text = "";
                            MessageBox.Show("Se intodujo numero no valido", "Error al registrar cliente", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                        ApellidoPaternoTextBox.Text = "";
                        MessageBox.Show("Se intodujo un apellido no valido", "Error al registrar cliente", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    ApellidoMaternoTextBox.Text = "";
                    MessageBox.Show("Se intodujo un apellido no valido", "Error al registrar cliente", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                NombreTextBox.Text = "";
                MessageBox.Show("Se intodujo un Nombre no valido", "Error al registrar cliente", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            return false;
        }
        public static bool EsEmailValido(string Email)
        {
            return Regex.IsMatch(Email, @"^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$");
        }
        public static bool EsNacionalidad(string nacionalidad)
        {
            return Regex.IsMatch(nacionalidad, @"[a-zA-ZñÑ\s]");
        }
        public static bool EsCodigoPostalValido(string postal)
        {
            return Regex.IsMatch(postal, @"^ ([1 - 9]{ 2}|[0 - 9][1 - 9] |[1 - 9][0 - 9])[0-9]{3}$");
        }
        public static bool EsNombre(string nombre)
        {
            return Regex.IsMatch(nombre, @"[a - zA - ZñÑ\s]{ 2,50}");
        }
        public static bool EsNumero(string numero)
        {
            return Regex.IsMatch(numero, @"[0-9]{1,9}(\.[0-9]{0,2})?$");
        }
        
        public void LimpiarCampos()
        {
            NombreTextBox.Text = "";
            ApellidoMaternoTextBox.Text = "";
            ApellidoPaternoTextBox.Text = "";
            EmailTextBox.Text = "";
            TelefonoTextBox.Text = "";
            NacionalidadTextBox.Text = "";
            NumeroNochesTextBox.Text = "";
            NumeroPersonasTextBox.Text = "";
            ImporteTextBox.Text = "";
            IVATextBox.Text = "";
            TotalTextBox.Text = "";
            RFCTextBox.Text = "";
            DireccionTextBox.Text = "";
            EstadoTextBox.Text = "";
            CodigoPostalTextBox.Text = "";
            HabitacionesListView.Items.Clear();
        }

        private void FormatearLista()
        {
            var lista = new GridView();
            HabitacionesListView.View = lista;

            AgregarColumna(lista, "IdHabitacion");
            AgregarColumna(lista, "Estilo");
            AgregarColumna(lista, "Tamano");
            AgregarColumna(lista, "Terraza");
            AgregarColumna(lista, "TipoHabitacion");
            AgregarColumna(lista, "Vista");
            AgregarColumna(lista, "Costo");

        }

        private void AgregarColumna(GridView grid, String nombreColumna)
        {
            grid.Columns.Add(new GridViewColumn
            {
                Header = nombreColumna,
                DisplayMemberBinding = new Binding(nombreColumna)
            });
        }

        public void Actualizar()
        {
            foreach (Habitacion habitacion in ListaHabitaciones)
            {
                HabitacionesListView.Items.Add(habitacion);
            }
        }

        public void Limpiar()
        {
            HabitacionesListView.Items.Clear();
        }

        private void CalcularButton_Click(object sender, RoutedEventArgs e)
        {
            var reservacion = new Reservacion();
            var habitacion = new Habitacion();
            if (string.IsNullOrEmpty(NumeroNochesTextBox.Text))
            {
                MessageBox.Show("El número de noches no debe estar vacío.", "Error al Cotizar", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (string.IsNullOrEmpty(NumeroPersonasTextBox.Text))
            {
                MessageBox.Show("El número de personas no debe estar vacío.", "Error al Cotizar", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (HabitacionesListView.Items.Count <= 0)
            {
                MessageBox.Show("La lista de habitaciones a cotizar no debe estar vacía.", "Error al Cotizar", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            try
            {
                reservacion.Noches = Int32.Parse(NumeroNochesTextBox.Text);
                reservacion.NumeroPersonas = Int32.Parse(NumeroPersonasTextBox.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("El tipo de dato instroducido no es válido, solo puede usar números enteros", "Error al Cotizar", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            double sumaHabitaciones = 0;

            foreach (Habitacion habitacion1 in ListaHabitaciones)
            {
                sumaHabitaciones += habitacion1.Costo;
            }
            double costopersonas = ((reservacion.NumeroPersonas - 1) * 345);
            double costohabitaciontotal = reservacion.Noches * sumaHabitaciones;
            double importe = (costohabitaciontotal + costopersonas);
            ImporteTextBox.Text = importe.ToString();
            double iva = importe * 0.16;
            double CostoTotal = importe + iva;
            IVATextBox.Text = iva.ToString();          
            TotalTextBox.Text = Math.Round(CostoTotal, 2).ToString();
            
        }
        public void SoloLetras(object sender, TextChangedEventArgs e)
        {
            string originalText = ((TextBox)sender).Text.Trim();
            if (originalText.Length > 0)
            {
                int inputOffset = e.Changes.ElementAt(0).Offset;
                try
                {
                    char inputChar = originalText.ElementAt(inputOffset);
                    if (!char.IsLetter(inputChar))
                    {
                        ((TextBox)sender).Text = originalText.Remove(inputOffset, 1);
                    }
                }catch(ArgumentOutOfRangeException)
                {
                    
                }
            }
        }
        public void SoloNumero(object sender, TextChangedEventArgs e)
        {
            string originalText = ((TextBox)sender).Text.Trim();
            if (originalText.Length > 0)
            {
                int inputOffset = e.Changes.ElementAt(0).Offset;
                try {
                    char inputChar = originalText.ElementAt(inputOffset);
                    if (!char.IsDigit(inputChar))
                    {
                        ((TextBox)sender).Text = originalText.Remove(inputOffset, 1);
                    }
                } catch (ArgumentOutOfRangeException)
                {

                }
            }
        }    

        private void ApellidoPaternoTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SoloLetras(sender, e);
        }
        private void NombreTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SoloLetras(sender, e);
        }
        private void ApellidoMaternoTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SoloLetras(sender, e);
        }
        private void TelefonoTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SoloNumero(sender, e);
        }
        private void NumeroNochesTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SoloNumero(sender, e);
        }
        private void NumeroPersonasTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SoloNumero(sender, e);
        }  
        private void NacionalidadTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SoloLetras(sender, e);
        }

        private void FacturaCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            RFCTextBox.IsEnabled = true;
            DireccionTextBox.IsEnabled = true;
            EstadoTextBox.IsEnabled = true;
            CodigoPostalTextBox.IsEnabled = true;
        }

        private void FacturaCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            RFCTextBox.IsEnabled = false;
            DireccionTextBox.IsEnabled = false;
            EstadoTextBox.IsEnabled = false;
            CodigoPostalTextBox.IsEnabled = false;
        }

        private void CodigoPostalTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SoloNumero(sender, e);
        }

        private void EstadoTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            SoloLetras(sender, e);
        }

        private void RemoverHabitacionButton_Click(object sender, RoutedEventArgs e)
        {
             if(HabitacionesListView.SelectedItem != null)
            {
                try
                {
                    List<int> indexSeleccionados = new List<int>();
                    foreach (Habitacion habitacion in HabitacionesListView.SelectedItems)
                    {
                        int index = HabitacionesListView.SelectedIndex;
                        ListaHabitaciones.RemoveAt(index);
                    }
                } catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    HabitacionesListView.Items.Clear();
                    Actualizar();
                }
                
            }   
        }
    }
}
