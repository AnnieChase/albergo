﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessLogic.DataAccess;
using BusinessLogic.Model;




namespace Albergo.Paginas
{
    /// <summary>
    /// Lógica de interacción para FormularioCotizacion.xaml
    /// </summary>
    public partial class FormularioCotizacion : Page
    {
        public ListView habitaciones;
        public List<Habitacion> ListaHabitaciones = new List<Habitacion>();
        private bool nonNumberEntered = false;
        public FormularioCotizacion()
        {
            InitializeComponent();
            habitaciones = HabitacionesListView;
            FormatearLista();
        }

        private void AnadirHabitacionButton_Click(object sender, RoutedEventArgs e)
        {
            ListarHabitaciones listarHabitaciones = new ListarHabitaciones(this, ListaHabitaciones);
            listarHabitaciones.ShowDialog();
            Limpiar();
            Actualizar();
        }

        public void Limpiar()
        {
            HabitacionesListView.Items.Clear();
        }

        public void Actualizar()
        {
            foreach(Habitacion habitacion in ListaHabitaciones)
            {
                HabitacionesListView.Items.Add(habitacion);
            }
        }

        private void FormatearLista()
        {
            var lista = new GridView();
            HabitacionesListView.View = lista;

            AgregarColumna(lista, "IdHabitacion");
            AgregarColumna(lista, "Estilo");
            AgregarColumna(lista, "Tamano");
            AgregarColumna(lista, "Terraza");
            AgregarColumna(lista, "TipoHabitacion");
            AgregarColumna(lista, "Vista");
            AgregarColumna(lista, "Costo");

        }

        private void AgregarColumna(GridView grid, String nombreColumna)
        {
            grid.Columns.Add(new GridViewColumn
            {
                Header = nombreColumna,
                DisplayMemberBinding = new Binding(nombreColumna)
            });
        }

        private void CotizarButton_Click(object sender, RoutedEventArgs e)
        {
            var cotizacion = new Cotizacion();
            var habitacion = new Habitacion();
            if (string.IsNullOrEmpty(NumeroNochesTextBox1.Text))
            {
                MessageBox.Show("El número de noches no debe estar vacío.", "Error al Cotizar", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (string.IsNullOrEmpty(NumeroPersonastext.Text))
            {
                MessageBox.Show("El número de personas no debe estar vacío.", "Error al Cotizar", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (HabitacionesListView.Items.Count <= 0)
            {
                MessageBox.Show("La lista de habitaciones a cotizar no debe estar vacía.", "Error al Cotizar", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            try
            {
                cotizacion.NochesEstancia = Int32.Parse(NumeroNochesTextBox1.Text);
                cotizacion.NumeroPersonas = Int32.Parse(NumeroPersonastext.Text);
            }
            catch (FormatException)
            {
                MessageBox.Show("El tipo de dato instroducido no es válido, solo puede usar números enteros", "Error al Cotizar", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            double sumaHabitaciones = 0;
           
            foreach (Habitacion habitacion1 in ListaHabitaciones)
            {
                sumaHabitaciones += habitacion1.Costo;
            }
            double costopersonas = ((cotizacion.NumeroPersonas - 1) * 345);
            double costohabitaciontotal = cotizacion.NochesEstancia * sumaHabitaciones;
            double importe = (costohabitaciontotal + costopersonas);
            ImporteTextBox.Text = importe.ToString();
            double iva = importe * 0.16;
            double CostoTotal = importe + iva;
            IVATextBox.Text = iva.ToString();
            TotalTextBox.Text = CostoTotal.ToString();
        }
        public void SoloLetras(object sender, TextChangedEventArgs e)
        {
            string originalText = ((TextBox)sender).Text.Trim();
            if (originalText.Length > 0)
            {
                int inputOffset = e.Changes.ElementAt(0).Offset;
                char inputChar = originalText.ElementAt(inputOffset);
                if (!char.IsLetter(inputChar))
                {
                    ((TextBox)sender).Text = originalText.Remove(inputOffset, 1);
                }
            }
        }
        public void SoloNumero(object sender, TextChangedEventArgs e)
        {
            string originalText = ((TextBox)sender).Text.Trim();
            if (originalText.Length > 0)
            {
                int inputOffset = e.Changes.ElementAt(0).Offset;
                char inputChar = originalText.ElementAt(inputOffset);
                if (!char.IsDigit(inputChar))
                {
                    ((TextBox)sender).Text = originalText.Remove(inputOffset, 1);
                }
            }
        }

        private void NumeroNochesTextBox1_TextChanged(object sender, TextChangedEventArgs e)
        {
            SoloNumero(sender,e);
        }

        private void NumeroPersonastext_TextChanged(object sender, TextChangedEventArgs e)
        {
            SoloNumero(sender,e);
        }

        private void RemoverHabitacionButton_Click(object sender, RoutedEventArgs e)
        {
            if (HabitacionesListView.SelectedItem != null)
            {
                try
                {
                    List<int> indexSeleccionados = new List<int>();
                    foreach (Habitacion habitacion in HabitacionesListView.SelectedItems)
                    {
                        int index = HabitacionesListView.SelectedIndex;
                        ListaHabitaciones.RemoveAt(index);
                    }
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                finally
                {
                    HabitacionesListView.Items.Clear();
                    Actualizar();
                }

            }
        }

     
    }
}
