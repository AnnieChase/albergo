﻿using Albergo.Paginas;
using BusinessLogic.DataAccess;
using BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Albergo
{
    /// <summary>
    /// Lógica de interacción para ListarHabitaciones.xaml
    /// </summary>
    public partial class ListarHabitaciones : Window
    {
        FormularioCotizacion formularioCotizacion;
        NuevaReservacion NuevaReservacion;
        List<Habitacion> habitaciones;
        List<Habitacion> habitacionesReservadas;

        public ListarHabitaciones(FormularioCotizacion formulario, List<Habitacion> HabitacionesReservadas)
        {
            InitializeComponent();
            FormatearLista();
            this.formularioCotizacion = formulario;
            habitacionesReservadas = new List<Habitacion>();
            habitacionesReservadas = HabitacionesReservadas;
            CargarDatos();
        }


        public ListarHabitaciones(NuevaReservacion reservacion, List<Habitacion> HabitacionesReservadas)
        {
            InitializeComponent();
            FormatearLista();
            this.NuevaReservacion = reservacion;
            habitacionesReservadas = new List<Habitacion>();
            habitacionesReservadas = HabitacionesReservadas;
            CargarDatos();
        }

        private void CancelarButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void CargarDatos()
        {
            HabitacionDAO habitacionDAO = new HabitacionDAO();
            habitaciones = habitacionDAO.RecuperarHabitaciones();
            List<Habitacion> indexYaReservadas = new List<Habitacion>();

            foreach(Habitacion habitacion in habitaciones)
            {
                if(habitacionesReservadas != null)
                {
                    foreach (Habitacion hab in habitacionesReservadas)
                    {
                        if (habitacion.IdHabitacion == hab.IdHabitacion)
                        {
                            indexYaReservadas.Add(habitacion);
                        }
                    }
                }
            }
            foreach(Habitacion habitacion in indexYaReservadas)
            {
                habitaciones.Remove(habitacion);
            }
            foreach(Habitacion habitacion in habitaciones)
            {
                HabitacionesListView.Items.Add(habitacion);
            }
    
        }

        private void FormatearLista()
        {
            var lista = new GridView();
            HabitacionesListView.View = lista;

            AgregarColumna(lista, "IdHabitacion");
            AgregarColumna(lista, "Estilo");
            AgregarColumna(lista, "Tamano");
            AgregarColumna(lista, "Terraza");
            AgregarColumna(lista, "TipoHabitacion");
            AgregarColumna(lista, "Vista");
            AgregarColumna(lista, "Costo");

        }

        private void AgregarColumna(GridView grid, String nombreColumna)
        {
            grid.Columns.Add(new GridViewColumn
            {
                Header = nombreColumna,
                DisplayMemberBinding = new Binding(nombreColumna)
            });
        }

        private void AnadirButton_Click(object sender, RoutedEventArgs e)
        {
            if (HabitacionesListView.SelectedItem == null)
            {
                MessageBox.Show("Seleccione un elemento de la lista", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            if (formularioCotizacion != null)
            {
                int index = HabitacionesListView.SelectedIndex;
                Console.WriteLine(index);
                Habitacion singlehabitacion = habitaciones[index];
                Console.WriteLine(singlehabitacion == null);
                formularioCotizacion.ListaHabitaciones.Add(singlehabitacion);
                
                Close();
            }
            else
            {
                if(HabitacionesListView.SelectedItem != null)
                {
                    int index = HabitacionesListView.SelectedIndex;
                    Console.WriteLine(index);
                    Habitacion singlehabitacion = habitaciones[index];
                    Console.WriteLine(singlehabitacion == null);
                    NuevaReservacion.ListaHabitaciones.Add(singlehabitacion);
                    Close();
                }
                
            }
        }


    }
}
