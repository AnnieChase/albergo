using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLogic.Model;
using BusinessLogic.DataAccess;


namespace Albergo
{
    /// <summary>
    /// Interaction logic for NuevaCotizacion.xaml
    /// </summary>
    public partial class NuevaCotizacion : Window
    {
        private CotizacionDAO cotizacionDAO = new CotizacionDAO();

        public NuevaCotizacion()
        {
            InitializeComponent();
            var tipoHabitacionDAO = new TipoHabitacionDAO();
            var tiposhabitaciones = tipoHabitacionDAO.GetTiposHabitaciones();
            foreach (var tipoHabitacion in tiposhabitaciones)
            {
                comboTipoHabitacion.Items.Add(tipoHabitacion.Nombre);
            }
        }

        private void ButtonGuardarCotizacion_Click(object sender, RoutedEventArgs e)
        {
            var habitacion = new Habitacion();
            var cotizacion = new Cotizacion();
            cotizacion.NochesEstancia = Int32.Parse(txtNoches.Text);
            cotizacion.NumeroPersonas = Int32.Parse(txtPersonas.Text);
            cotizacion.TipoHabitacion = comboTipoHabitacion.Text;
            cotizacion.CostoTotal = (((cotizacion.NochesEstancia*((cotizacion.NumeroPersonas-1)*345))*0.16)+habitacion.costo+(cotizacion.NochesEstancia*((cotizacion.NumeroPersonas-1)*345)));
            txtImporte.Text = (habitacion.costo + (cotizacion.NochesEstancia * ((cotizacion.NumeroPersonas - 1) * 345))).ToString();
            txtIVA.Text = ((cotizacion.NochesEstancia * ((cotizacion.NumeroPersonas - 1) * 345)) * 0.16).ToString();
            txtTotal.Text = (cotizacion.CostoTotal).ToString();

            cotizacionDAO.AgregarCotizacion(cotizacion);
        }
    }
}
