GO
CREATE TABLE persona (
	idPersona INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	nombre VARCHAR(30) NOT NULL,
	apellidoPaterno VARCHAR (25) NOT NULL,
	apellidoMaterno VARCHAR (25) NOT NULL,
	telefono VARCHAR(13) NOT NULL,
	email VARCHAR(60) NOT NULL
);
GO

GO
CREATE TABLE cliente (
	idCliente INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	codigoPostal VARCHAR(15),
	direccion VARCHAR(80),
	estado VARCHAR(30),
	nacionalidad VARCHAR(40) NOT NULL,
	rfc VARCHAR(15),
	idPersona INT NOT NULL FOREIGN KEY REFERENCES persona(idPersona)
);
GO

GO
CREATE TABLE empleado (
	idEmpleado INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	clavePersonal VARCHAR(10) NOT NULL
);
GO

GO
CREATE TABLE cliente_persona (
	idClientePersona INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	idCliente INT NOT NULL FOREIGN KEY REFERENCES cliente(idCliente),
	idPersona INT NOT NULL FOREIGN KEY REFERENCES persona(idPersona)
);
GO

GO
CREATE TABLE empleado_persona (
	idEmpleadoPersona INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	idEmpleado INT NOT NULL FOREIGN KEY REFERENCES empleado(idEmpleado),
	idPersona INT NOT NULL FOREIGN KEY REFERENCES persona(idPersona)
);
GO

GO
CREATE TABLE habitacion (
	idHabitacion VARCHAR(5) PRIMARY KEY NOT NULL,
	costo DECIMAL(5,2) NOT NULL,
	estilo VARCHAR(30) NOT NULL,
	tamanio VARCHAR(30) NOT NULL,
	terraza VARCHAR(30) NOT NULL,
	tipoHabitacion VARCHAR(30) NOT NULL,
	vista VARCHAR(30) NOT NULL,
	ocupado bit NOT NULL
);
GO

GO
CREATE TABLE reservacion (
	idReservacion INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	fechaReservacion DATE NOT NULL,
	noches INT NOT NULL,
	costoCalculado DECIMAL(6,2),
	idCliente INT NOT NULL FOREIGN KEY REFERENCES cliente(idCliente),
	idEmpleado INT NOT NULL FOREIGN KEY REFERENCES empleado(idEmpleado)
);
GO

GO
CREATE TABLE habitacionesReservadas (
	idHbitacionReservada INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
	idReservacion INT NOT NULL FOREIGN KEY REFERENCES reservacion(idReservacion),
	idHabitacion VARCHAR(5) NOT NULL FOREIGN KEY REFERENCES habitacion(idHabitacion)
);
GO

GO
CREATE PROCEDURE crearCliente
	@nombre VARCHAR(30),
	@apellidoPaterno VARCHAR(25),
	@apellidoMaterno VARCHAR(25),
	@telefono VARCHAR(13),
	@email VARCHAR(60),
	@codigoPostal VARCHAR(15),
	@direccion VARCHAR(80),
	@estado VARCHAR(30),
	@nacionalidad VARCHAR(40),
	@rfc VARCHAR(15)
	
	AS

	INSERT INTO persona (nombre, apellidoPaterno, apellidoMaterno, telefono, email) 
	VALUES (@nombre, @apellidoPaterno, @apellidoMaterno, @telefono, @email);

	INSERT INTO cliente (codigoPostal, direccion, estado, nacionalidad, rfc)
	VALUES (@codigoPostal, @direccion, @estado, @nacionalidad, @rfc);
GO

GO
CREATE PROCEDURE relacionarClientePersona
	@idCliente INT,
	@idPersona INT

	AS

	INSERT INTO cliente_persona (idCliente, idPersona)
	VALUES (@idCliente, @idPersona);
GO


GO
CREATE PROCEDURE crearEmpleado
	@nombre VARCHAR(30),
	@apellidoPaterno VARCHAR(25),
	@apellidoMaterno VARCHAR(25),
	@telefono VARCHAR(13),
	@email VARCHAR(60),
	@clavePersonal VARCHAR(10)
	
	AS

	INSERT INTO persona (nombre, apellidoPaterno, apellidoMaterno, telefono, email) 
	VALUES (@nombre, @apellidoPaterno, @apellidoMaterno, @telefono, @email);

	INSERT INTO empleado (clavePersonal)
	VALUES (@clavePersonal);
GO

GO
CREATE PROCEDURE relacionarEmpleadoPersona
	@idEmpleado INT,
	@idPersona INT

	AS

	INSERT INTO empleado_persona (idEmpleado, idPersona)
	VALUES (@idEmpleado, @idPersona);
GO

GO
CREATE PROCEDURE crearReservacion
	@fechaReservacion DATE,
	@noches INT,
	@costoCalculado DECIMAL(6,2),
	@idCliente INT,
	@idEmpleado INT

	AS

	INSERT INTO reservacion (fechaReservacion, noches, costoCalculado, idCliente, idEmpleado)
	VALUES (@fechaReservacion, @noches, @costoCalculado, @idCliente, @idEmpleado);
GO

GO
CREATE PROCEDURE reservarHabitaciones
	@idHabitacion VARCHAR(5),
	@ultimoIdReservacion INT

	AS 
	INSERT INTO habitacionesReservadas (idReservacion, idHabitacion)
	VALUES (@ultimoIdReservacion, @idHabitacion);
GO

GO
CREATE PROCEDURE getLastIdCliente
	AS
	SELECT IDENT_CURRENT('cliente');
GO

CREATE PROCEDURE getLastIdEmpleado
	AS
	SELECT IDENT_CURRENT('empleado');
GO

CREATE PROCEDURE getLastPersona
	AS
	SELECT IDENT_CURRENT('persona');
GO

CREATE PROCEDURE getLastReservacion
	AS
	SELECT IDENT_CURRENT('reservacion');
GO